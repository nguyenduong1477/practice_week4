class Product {
  late String imageUrl;
  late String name;
  late double price;
  late String description;

  Product({
    required this.imageUrl,
    required this.name,
    required this.price,
    required this.description,
  });
}

final List<Product> products = [
  Product(
    imageUrl: 'assets/image/beatsStudio3.jpg',
    name: 'Beats Studio 3',
    price: 79.95,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/g102.jpg',
    name: 'Logitech G102',
    price: 109.99,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/gk10.jpg',
    name: 'GK10 Keyboard',
    price: 1199.99,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/hpPavilion.jpg',
    name: 'Laptop HP Pavilion',
    price: 88.99,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/lenovoLegion.jpg',
    name: 'Laptop Lenovo Legion 5',
    price: 99.95,
    description:
    '',
  ),
];

final List<Product> others = [
  Product(
    imageUrl: 'assets/image/m190.png',
    name: 'Logitech M190',
    price: 18.49,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/macbookAir.jpg',
    name: 'Macbook Air',
    price: 18.40,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/proX.png',
    name: 'ProX Keyboard',
    price: 18.98,
    description:
    '',
  ),
  Product(
    imageUrl: 'assets/image/ramKingston.jpg',
    name: 'RAM Kingston 4GB',
    price: 10.20,
    description:
    '',
  ),
];

final List<Product> cart = [
  products[3],
  others[2],
  products[1],
  others[0],
  products[4],
];
