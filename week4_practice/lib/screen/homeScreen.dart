import 'package:flutter/material.dart';
import 'package:week4_practice/screen/loginScreen.dart';
import '../model/productModel.dart';
import '../screen/cartScreen.dart';
import '../widget/product.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({ Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 5.0,
        leading: Padding(
          padding: EdgeInsets.only(left: 20),
          child: InkResponse(
            onTap: () {
              Navigator.push(context,MaterialPageRoute(builder: (_)=>HomeScreen(key: null,)));
            },
            child: Icon(
              Icons.menu_open_outlined,
              size: 30,
              color: Colors.red,
            ),
          ),
        ),
        title: Padding(
          padding: const EdgeInsets.only(right: 18.0, top: 10),
          child: Image(
            image: AssetImage("assets/image/gearvnLogo.webp"),
            height: 30,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 10, right: 20),
                child: InkResponse(
                  onTap: () {
                    Navigator.push(context,MaterialPageRoute(builder: (_)=>CartScreen(key: null,)));
                  },
                  child: Icon(
                    Icons.shopping_bag_outlined,
                    size: 30,
                    color: Colors.red,
                  ),
                ),
              ),
              Positioned(
                bottom: 8,
                right: 16,
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.orange,
                  ),
                  child: Center(
                    child: Text(
                      '5',
                      style: TextStyle(
                          fontWeight: FontWeight.w500, color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(right: 20),
            child: InkResponse(
              onTap: () {},
              child: Icon(
                Icons.search_outlined,
                size: 30,
                color: Colors.red,
              ),
            ),
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Image(
                image: AssetImage("assets/image/gearvn.png"),
              ),
            ],
          ),
          SizedBox(height: 15,),
          ProductCarousal(title: "Gearvn Special Offers",products: products,),
          ProductCarousal(title: "Others",products: others,),

        ],
      ),
    );
  }
}
