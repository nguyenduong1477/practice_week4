import 'package:flutter/material.dart';
import 'package:week4_practice/screen/homeScreen.dart';
import 'package:week4_practice/screen/loginScreen.dart';
import '../screen/homeScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Amazon',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryIconTheme: Theme.of(context).primaryIconTheme.copyWith(
          color: Colors.yellow,
        ),
      ),
      home: LoginScreen(),
    );
  }
}